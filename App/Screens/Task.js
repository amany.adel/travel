import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';

import {getTask} from '../Actions';
import {connect} from 'react-redux';
import {
  //   CarouselPart,
  TrainerSection,
  button,
  TitleSection,
  Border,
} from '../Components';
import Carousel from 'pinar';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RTLView, RTLText} from 'react-native-rtl-layout';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import Moment from 'moment';
import openMap from 'react-native-open-maps';
import Orientation from 'react-native-orientation';

console.disableYellowBox = true;

class Task extends Component {
  constructor(props) {
    super(props);
    this.props.getTask();
    this.state = {
      datacarousel: {
        autoplayInterval: 6000,
        width: '95%',
        height: hp(30),
        content: this.props.sliderData,
      },
      latitude: this.props.taskData.latitude,
      longitude: this.props.taskData.longitude,
    };
  }
  _goToMap() {
    console.log('map');
    //   console.log(this.state.latitude)
    openMap({latitude: '24.7535346921672', longitude: '46.5850353240967'});
  }
  componentDidMount() {
    Orientation.lockToPortrait();
  }
  //   getAction() {
  //     this.props.getTask();
  //   }

  render() {
    // {
    //   this.getAction.bind(this);
    // }
    return (
      <ScrollView style={styles.container}>
        <Carousel
          showsDots="false"
          loop="true"
          showsControls="false"
          autoplay="true"
          autoplayInterval={this.state.datacarousel.autoplayInterval}
          height={this.state.datacarousel.height}
          dotsContainerStyle={styles.dotsContainerStyle}
          activeDotStyle={styles.activeDotStyle}
          dotStyle={styles.dotStyle}
          renderNext={({scrollToNext}): JSX.Element => (
            <TouchableOpacity
              style={styles.iconsSliderNext}
              accessibilityRole="button"
              onPress={scrollToNext}
              testID="custom-next">
              <Text style={styles.buttonNext}>
                <Icon name="chevron-right" size={25} color="white" />
              </Text>
            </TouchableOpacity>
          )}
          controlsButtonStyle={styles.controlsButtonStyle}
          // renderDots={({ index, total }): JSX.Element => (
          //     <View style={styles.dotsContainer}>
          //       <Text testID="custom-dots" style={styles.dotsText}>
          //         {`${index + 1}/${total}`}
          //       </Text>
          //     </View>
          //   )}
        >
          {this.props.taskData.img.map(element => {
            return (
              <View>
                <ImageBackground
                  source={{
                    uri: element,
                  }}
                  style={{width: '100%', height: '100%'}}>
                  <View style={styles.iconsSlider}>
                    <Text style={styles.iconText}>
                      <Icon name="star" size={25} color="white" />
                    </Text>
                    <Text style={styles.iconText}>
                      <Icon name="share-alt" size={25} color="white" />
                    </Text>
                  </View>
                </ImageBackground>

                {/* <Image
        style={{width:'100%',height:'100%'}}
        source={{
          uri: element,
        }}
      /> */}
              </View>
            );
          })}
        </Carousel>
        <TrainerSection>
          <View style={styles.trainer}>
            <Text style={styles.headerText}>
              {this.props.taskData.interest}
            </Text>

            <Text style={{textAlign: 'right'}}>
              <Icon name="hashtag" size={15} color="#b2b1b8" />
            </Text>
          </View>
        </TrainerSection>
        <TrainerSection>
          <Text locale="ar" style={styles.headerText}>
            الاسم الكامل للدورة بشكل افتراضي من اجل اظهار شكل التصميم
          </Text>
        </TrainerSection>
        <TrainerSection>
          <View style={styles.trainer}>
            <Text style={styles.headerText}>
              {Moment(this.props.taskData.date).format(
                'EEEE, MMMM d, yyyy h:mm:ss',
              )}
            </Text>

            <Text style={{textAlign: 'right'}}>
              <Icon name="table" size={20} color="#b2b1b8" />
            </Text>
          </View>
          <View style={styles.trainer}>
            {/* <TouchableOpacity 
        onPress={this._goToMap}
      > */}

            <Text style={styles.headerText}>
              عنوان الحدث او الدوره بشكل كامل
            </Text>
            {/* </TouchableOpacity> */}

            <Text>
              <Icon name="map-marker" size={20} color="#b2b1b8" />
            </Text>
          </View>
        </TrainerSection>
        <Border />
        <TrainerSection>
          <RTLView locale="ar" style={styles.trainer}>
            <RTLText style={styles.headerText}>
              {this.props.taskData.trainerName}
            </RTLText>
          </RTLView>

          <RTLText locale="ar" style={styles.bodyText}>
            {' '}
            {this.props.taskData.trainerInfo}{' '}
          </RTLText>
        </TrainerSection>

        <Border />

        <TrainerSection>
          {/* <Text style={styles.headerText}>fdf</Text> */}
          <RTLText locale="ar" style={styles.headerText}>
            عن الدورة
          </RTLText>
          <RTLText locale="ar" style={styles.bodyText}>
            {' '}
            {this.props.taskData.occasionDetail}{' '}
          </RTLText>

          {/* <Text style={styles.bodyText}>fdf</Text> */}
        </TrainerSection>

        <Border />
        <TrainerSection>
          {/* <Text style={styles.headerText}>fdf</Text> */}
          <RTLText locale="ar" style={styles.headerText}>
            عن الدورة
          </RTLText>

          {/* <Text style={styles.bodyText}>fdf</Text> */}
        </TrainerSection>
        <RTLView
          locale="ar"
          style={{
            flex: 1,
            flexDirection: 'row',
            // justifyContent: 'space-around',
            justifyContent: 'space-between',
          }}>
          <RTLView locale="ar" style={styles.priceBody}>
            <RTLText style={styles.priceText}>الحجز العادي</RTLText>
          </RTLView>
          <RTLView locale="ar" style={styles.priceBody}>
            <RTLText style={styles.priceText}>
              SAR {this.props.taskData.price}
            </RTLText>
          </RTLView>
        </RTLView>
        <RTLView
          locale="ar"
          style={{
            flex: 1,
            flexDirection: 'row',
            // justifyContent: 'space-around',
            justifyContent: 'space-between',
          }}>
          <RTLView locale="ar" style={styles.priceBody}>
            <RTLText style={styles.priceText}>الحجز العادي</RTLText>
          </RTLView>
          <RTLView locale="ar" style={styles.priceBody}>
            <RTLText style={styles.priceText}>
              SAR {this.props.taskData.price}
            </RTLText>
          </RTLView>
        </RTLView>

        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#702f7f', '#803793', '#923fab']}
          style={styles.button}>
          <TouchableOpacity
          // style={styles.button}
          >
            <RTLView locale="ar">
              <RTLText locale="ar" style={styles.buttonText}>
                قم بالحجز الان
              </RTLText>
            </RTLView>
          </TouchableOpacity>
        </LinearGradient>

        {/* <Text>{this.props.taskData.title}</Text> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#ffffff'},
  headerText: {
    color: '#b2b1b8',
    fontSize: hp('2.5%'),
    paddingBottom: hp(1),
    fontWeight: 'bold',
    paddingHorizontal: hp(1),
    alignContent: 'flex-end',
    fontFamily: 'Cairo',
  },
  bodyText: {
    color: '#d4d4d6',
    fontSize: hp('2.5%'),
    fontWeight: 'bold',
    fontFamily: 'Cairo-Light',
  },
  button: {
    flex: 1,
    backgroundColor: '#8e3ba5',
    flexDirection: 'row',
    // alignItems: "center",
    justifyContent: 'center',
    marginTop: hp(2),
  },
  buttonText: {
    color: '#ffffff',
    // flexDirection: "row",

    paddingVertical: hp(1.5),
    fontSize: hp('2.5%'),
    fontWeight: 'bold',
    alignItems: 'center',
  },
  text: {
    color: '#1f2d3d',
    opacity: 0.7,
    fontSize: 48,
    fontWeight: 'bold',
  },
  dotsContainerStyle: {
    position: 'absolute',
    bottom: hp(3),
    left: hp(3),
    right: 0,
    display: 'flex',
    flexDirection: 'row',
  },
  activeDotStyle: {
    backgroundColor: '#fff8e6',
    width: wp(2),
    height: hp(1.5),
    // width: wp(5),
    // height: hp(5),
    borderRadius: 100 / 2,

    marginHorizontal: hp(0.5),
    marginVertical: hp(0.5),
  },
  dotStyle: {
    backgroundColor: '#f1dcc7',
    width: wp(1.5),
    height: hp(1),
    borderRadius: 100 / 2,

    marginHorizontal: hp(0.5),
    marginVertical: hp(0.5),
  },
  controlsButtonStyle: {
    opacity: -1,
    color: 'white',
  },
  priceSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  priceTitle: {
    flex: 0.5,
    backgroundColor: 'red',
  },
  iconsSlider: {
    position: 'absolute',
    top: hp(3),
    left: 0,
    right: 0,
    display: 'flex',
    flexDirection: 'row',
    // paddingVertical:hp(1),
    paddingHorizontal: hp(1.5),
  },
  iconsSliderNext: {
    position: 'absolute',
    top: hp(3),
    left: wp(86),
    right: 0,
    display: 'flex',
    flexDirection: 'row',

    // paddingVertical:hp(1),
    paddingHorizontal: hp(1.5),
  },

  iconText: {
    paddingHorizontal: hp(1),
    paddingVertical: hp(1.5),
  },
  dotsText: {
    fontSize: 24,
    color: '#1f2d3d',
  },
  buttonNext: {
    paddingHorizontal: hp(1),
    paddingVertical: hp(1.5),
  },
  trainer: {
    flex: 1,
    flexDirection: 'row',
    // paddingHorizontal:hp(1),

    // justifyContent: 'space-around',
  },
  priceBody: {
    paddingHorizontal: hp(1.8),
  },
  priceText: {
    color: '#d4d4d6',
    fontSize: hp('2.5%'),
    fontWeight: 'bold',
    fontFamily: 'Cairo-Light',
  },
});

const mapStateToProps = state => {
  return {
    taskData: state.task.taskData,
  };
};

const TaskRedux = connect(
  mapStateToProps,
  {
    getTask,
  },
)(Task);
export {TaskRedux as Task};
