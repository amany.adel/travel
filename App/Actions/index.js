import {
    GET_Task_SUCCESS,
    GET_Task_FAIL
  } from './types';
  import Api from '../Api/api';

export const getTask = data => {
    console.log('task');
    return dispatch => {
      Api.GetTask(data).then(res => {
        if (res) dispatch({type: GET_Task_SUCCESS, payload: res});
        else dispatch({type: GET_Task_FAIL, payload: 'fail data'});
      });
    };
  };