
import React from 'react';
import { View ,Text} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RTLView, RTLText } from 'react-native-rtl-layout'

const Border = () => {
    return (
        <Text style={styles.containerStyle}>
        
        </Text>
    );
};

const styles = {
    containerStyle: {
        flex: 1,
        borderBottomWidth:wp(.6),
        borderBottomColor: '#f7f7f7',
        borderBottomStyle: 'solid'
        
    }
};

export { Border };
