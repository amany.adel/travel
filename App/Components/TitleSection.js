import React from 'react';
import { View } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RTLView, RTLText } from 'react-native-rtl-layout'

const TitleSection = (props) => {
    return (
        <RTLView locale='ar' style={styles.containerStyle}>
        {/* // <View style={styles.containerStyle}> */}
            {props.children}

        {/* </View> */}
        </RTLView>
    );
};

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        // justifyContent: 'space-around',
        justifyContent: 'space-between',
        // alignSelf:'flex-end',

        paddingVertical:hp(2),
        paddingHorizontal:hp(2),
        // borderBottomColor:'black',
        // borderWidth:1,
        // borderBottomColor: '#fcfcfc',
        // borderBottomColor: 'red',

        // borderBottomWidth: wp(1),
        borderBottomWidth: 1,
        borderBottomColor: 'red',
        borderBottomStyle: 'solid'
        
    }
};

export { TitleSection };