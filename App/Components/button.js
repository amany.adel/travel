import React from 'react';
import { View ,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RTLView, RTLText } from 'react-native-rtl-layout'

const Button = ({children, onPress, color, style}) => {
    return (
        <RTLView locale='ar' style={styles.containerStyle}>
        {/* // <View style={styles.containerStyle}> */}
			<TouchableOpacity
				onPress={onPress}
				useForeground={false}
				borderless={true}
			>
				<View style={styles.buttonStyle}>
					{children}
				</View>
			</TouchableOpacity>

        {/* </View> */}
        </RTLView>
    );
};

const styles = {
    buttonStyle: {
        width: wp(100),
        
    }
};

export { Button };