import React from 'react';
import { View } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RTLView, RTLText } from 'react-native-rtl-layout'

const TrainerSection = (props) => {
    return (
        <RTLView locale='ar' style={styles.containerStyle}>
        {/* // <View style={styles.containerStyle}> */}
            {props.children}

        {/* </View> */}
        </RTLView>
    );
};

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'column',
        // justifyContent: 'space-around',
        justifyContent: 'space-between',
        alignSelf:'flex-end',

        // paddingVertical:hp(1),
        paddingHorizontal:hp(1.8),
        marginVertical:hp(1.5),

        // borderBottomColor:'black',
        // borderWidth:1
       
        
    }
};

export { TrainerSection };