import {
    GET_Task_SUCCESS,
    GET_Task_FAIL

    } from '../Actions/types'

const INITIAL_STATE={
    taskData:{
    "id": "",
    "title": " ",
    "img": [
        
    ],
    "interest": "",
    "price": "",
    "date": "",
    "address": " ",
    "trainerName": " ",
    "trainerImg": "",
    "trainerInfo": " ",
    "occasionDetail": "",
    "latitude": "",
    "longitude": "",
    "isLiked": '',
    "isSold": '',
    "isPrivateEvent": '',
    "hiddenCashPayment": '',
    "specialForm": '',
    "questionnaire": '',
    "questExplanation": '',
    "reservTypes":[]
}

};

export default (state = INITIAL_STATE , action)=>{
    console.log(action);

    switch(action.type)
    {
        case GET_Task_SUCCESS:
            return {...state,taskData:action.payload};
        case GET_Task_FAIL:
            return {...state, taskData:action.payload};    
            
        default:
            return state;
    }

};