/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native';

import { Provider } from 'react-redux'
import {createStore , applyMiddleware} from 'redux';
import reducers from './App/Reducer'
import ReduxThunk from 'redux-thunk';
import {Task} from './App/Screens/index';
 

class App extends React.Component {
 
  render() {

      const store= createStore(reducers, {} , applyMiddleware(ReduxThunk));

      return (
        <Provider store={store}>
         <Task/>
          
        </Provider>
        
      );
  }
}

export default App;
